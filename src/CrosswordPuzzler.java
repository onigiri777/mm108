import java.util.Comparator;
import java.util.HashSet;
import java.util.Random;
import java.util.stream.IntStream;

public class CrosswordPuzzler {

    private static int[] scores = new int[]{0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144};
    private static final long TIME_LIMIT = 900;
    private static final char EMPTY = '-';

    private final Random random = new Random(893_893_1);
    private int width;
    private int height;
    private String[] words;
    private long startTime;
    private int[] jumpSequence;

    public String[] createPuzzle(int width, int height, String[] words) {
        this.startTime = System.currentTimeMillis();
        this.width = width;
        this.height = height;
        this.words = words;
        this.jumpSequence = createJumpSequence(height, width);

        State optimalState = createState();
        while (isTimeRemained()) {
            final State state = createState();
            for (int i = 0; i < words.length && isTimeRemained(); i++) {
                state.update();
            }
            optimalState = max(optimalState, state);
        }

        System.out.println(optimalState.score);
        return optimalState.toResult();
    }

    private int[] createJumpSequence(int height, int width) {
        final int[] jumpSequence = new int[((height + 1) / 2) * ((width + 1) / 2)];
        int index = 0;
        for (int h = 0; h < height; h += 2) {
            for (int w = 0; w < width; w += 2) {
                jumpSequence[index++] = h * width + w;
            }
        }
        return jumpSequence;
    }

    private State createState() {
        return State.of(words, height, width, random, jumpSequence);
    }

    private static class State {
        private final Random random;
        private final String[] words;
        private final boolean[] usedWords;
        private final int[] mapper;
        private final char[][] puzzle;
        private final int[][] counts;
        private final int[] jumpSequence;
        private final int height;
        private final int width;
        private int score;
        private int wordIndex = 0;

        private State(Random random, String[] words, boolean[] usedWords, int[] mapper, char[][] puzzle, int[][] counts, int[] jumpSequence, int height, int width, int score) {
            this.random = random;
            this.words = words;
            this.usedWords = usedWords;
            this.mapper = mapper;
            this.puzzle = puzzle;
            this.counts = counts;
            this.jumpSequence = jumpSequence;
            this.height = height;
            this.width = width;
            this.score = score;
        }

        private static State of(String[] words, int height, int width, Random random, int[] jumpSequence) {
            final boolean[] usedWords = new boolean[words.length];
            final char[][] puzzle = new char[height][width];
            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    puzzle[i][j] = EMPTY;
                }
            }
            final int[][] counts = new int[height][width];
            final int initialScore = -height * width;
            final int[] randomSequence = createSequence(random, words.length);
            final int[] mapper = IntStream.range(0, words.length)
                    .boxed()
                    .sorted(Comparator.comparing(index -> -words[(int) index].length())
                            .thenComparing(index -> randomSequence[(int) index]))
                    .mapToInt(x -> x)
                    .toArray();
            return new State(random, words, usedWords, mapper, puzzle, counts, jumpSequence, height, width, initialScore);
        }

        private String[] toResult() {
            final String[] result = new String[height];
            for (int i = 0; i < height; i++) {
                result[i] = new String(puzzle[i]);
            }
            return result;
        }

        private void update() {
            final int mappedIndex = mapper[wordIndex];
            final String word = words[mappedIndex];
            for (int rand : jumpSequence) {
                final int h = rand / width;
                final int w = rand % width;
                final boolean vertically = random.nextBoolean();
                if (canSetWord(h, w, vertically, word)) {
                    final int duplicate = setWord(h, w, vertically, word);
                    score += scores[word.length()] + word.length() - duplicate;
                    break;
                }
                if (canSetWord(h, w, !vertically, word)) {
                    final int duplicate = setWord(h, w, !vertically, word);
                    score += scores[word.length()] + word.length() - duplicate;
                    break;
                }
            }

            wordIndex++;
        }

        private boolean canSetWord(int h, int w, boolean vertically, String word) {
            if (vertically) {
                if (h + word.length() > height) {
                    return false;
                } else if (h - 1 >= 0 && puzzle[h - 1][w] != EMPTY) {
                    return false;
                } else if (h + word.length() < height && puzzle[h + word.length()][w] != EMPTY) {
                    return false;
                }
            } else {
                if (w + word.length() > width) {
                    return false;
                } else if (w - 1 >= 0 && puzzle[h][w - 1] != EMPTY) {
                    return false;
                } else if (w + word.length() < width && puzzle[h][w + word.length()] != EMPTY) {
                    return false;
                }
            }

            int nh = h;
            int nw = w;
            for (int i = 0; i < word.length(); i++) {
                if (puzzle[nh][nw] != EMPTY && puzzle[nh][nw] != word.charAt(i)) {
                    return false;
                }
                if (puzzle[nh][nw] == EMPTY) {
                    if (vertically) {
                        if (nw - 1 >= 0 && puzzle[nh][nw - 1] != EMPTY) {
                            return false;
                        } else if (nw + 1 < width && puzzle[nh][nw + 1] != EMPTY) {
                            return false;
                        }
                    } else {
                        if (nh - 1 >= 0 && puzzle[nh - 1][nw] != EMPTY) {
                            return false;
                        } else if (nh + 1 < height && puzzle[nh + 1][nw] != EMPTY) {
                            return false;
                        }
                    }
                }

                if (vertically) {
                    nh++;
                } else {
                    nw++;
                }
            }

            return true;
        }

        private int setWord(int h, int w, boolean vertically, String word) {
            int duplicate = 0;
            for (int i = 0; i < word.length(); i++) {
                puzzle[h][w] = word.charAt(i);
                if (counts[h][w] > 0) {
                    duplicate++;
                }
                counts[h][w]++;
                if (vertically) {
                    h++;
                } else {
                    w++;
                }
            }
            return duplicate;
        }

        private void unsetWord(int h, int w, boolean vertically, String word) {
            for (int i = 0; i < word.length(); i++) {
                counts[h][w]--;
                if (counts[h][w] == 0) {
                    puzzle[h][w] = EMPTY;
                }
                if (vertically) {
                    h++;
                } else {
                    w++;
                }
            }

        }

    }

    private boolean isTimeRemained() {
        return System.currentTimeMillis() - startTime < TIME_LIMIT;
    }

    private static State max(State a, State b) {
        if (a.score > b.score) {
            return a;
        } else {
            return b;
        }
    }

    private static int[] createSequence(Random random, int length) {
        final int[] sequence = new int[length];
        for (int i = 0; i < length; i++) {
            sequence[i] = i;
        }
        for (int i = 1; i < length; i++) {
            final int rand = random.nextInt(length - i + 1);
            final int tmp = sequence[length - i];
            sequence[length - i] = sequence[rand];
            sequence[rand] = tmp;
        }
        return sequence;
    }


}
