import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.util.*;

public class TestGenerator {

    private int width, height;
    private HashSet<String> wordList = new HashSet<>();
    private String[] words;


    public static void main(String[]args){
        for(int i=0;i<1000;i++) {
            final TestGenerator generator = new TestGenerator();
            generator.generateTestCase(i);
            generator.save(i);
        }
    }

    private void save(long i) {
        try{
            final Path path = Paths.get("C:\\Users\\Madao\\MM109\\test\\" + i + ".txt");
            final List<String> inputs = new ArrayList<>();
            inputs.add(Integer.toString(width));
            inputs.add(Integer.toString(height));
            inputs.addAll(Arrays.asList(words));
            Files.write(path,inputs);
        }catch (IOException e){
            throw new UncheckedIOException(e);
        }
    }

    private void generateTestCase(long seed) {
        if (seed == 1 || seed == 2) {
            if (seed == 1) {
                height = 3;
                width = 8;
            } else {
                height = 4;
                width = 7;
            }
            words = new String[] { "CLOCK", "BIO", "SWITCH", "CIS", "LOW", "CAT", "DOG", "TURTLE", "LION", "MULE", "ANT", "ELEPHANT", "POSSUM", "TURKEY" };
            wordList.addAll(Arrays.asList(words));
            return;
        }
        Random r = new Random(seed);
        try {
            r = SecureRandom.getInstance("SHA1PRNG");
            r.setSeed(seed);
        } catch (Exception e) {
        }
        width = r.nextInt(41) + 10;
        height = r.nextInt(41) + 10;
        int numWords = height * width / 4 + r.nextInt(1 + height * width - height * width / 4);
        String[] dist = new String[27];
        for (int i = 0; i < 27; i++) {
            char c = 'A';
            dist[i] = "";
            while (c <= 'Z') {
                if (r.nextDouble() < 0.6) {
                    dist[i] += c;
                } else {
                    c++;
                }
            }
        }
        char lastChar;
        int[] lenDist = new int[]{ 1, 1, 2, 2, 3, 4 };
        while (wordList.size() < numWords) {
            int wordLen = lenDist[r.nextInt(6)] + lenDist[r.nextInt(6)] + lenDist[r.nextInt(6)];
            String thisWord = "" + (lastChar = dist[26].charAt(r.nextInt(dist[26].length())));
            while (thisWord.length() < wordLen)
                thisWord += lastChar = dist[lastChar - 'A'].charAt(r.nextInt(dist[lastChar - 'A'].length()));
            wordList.add(thisWord);
        }
        words = wordList.toArray(new String[0]);
    }


}
