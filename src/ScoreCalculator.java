import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ScoreCalculator {

    public static void main(String[] args) throws IOException {
        new ScoreCalculator().calculate();
    }

    private void calculate() throws IOException {
        final File[] files = new File("resource").listFiles();
        final Map<String, List<Integer>> scores = new HashMap<>();
        final int[] bests = new int[99];
        for (int i = 0; i < bests.length; i++) {
            bests[i] = 0;
        }
        for (File file : files) {
            final Path path = file.toPath();
            final String key = file.toString().replace("resource\\", "").replace(".text", "");
            final List<Integer> values = Files.lines(path)
                    .map(line -> line.replaceAll("\\t", ""))
                    .map(line -> line.replaceAll(" ", ""))
                    .map(line -> line.replaceAll(":", ""))
                    .map(line -> line.replaceAll("[a-z]", ""))
                    .map(line -> line.split(",")[1])
                    .map(Integer::parseInt)
                    .collect(Collectors.toList());
            for (int i = 0; i < bests.length; i++) {
                bests[i] = Math.max(bests[i], values.get(i));
            }
            scores.put(key, values);
        }
        scores.keySet()
                .stream()
                .sorted()
                .map(key -> key + " " + calculateScore(bests, scores.get(key)))
                .forEach(System.out::println);

    }

    private String calculateScore(int[] bests, List<Integer> scores) {
        double score = 0;
        for (int i = 0; i < bests.length; i++) {
            score += (double) scores.get(i) / bests[i];
        }
        score /= bests.length;
        score *= 1_000_000;
        return Integer.toString((int) score);
    }
}
