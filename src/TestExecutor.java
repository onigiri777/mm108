import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class TestExecutor {

    private static final int START_SEED = 1;
    private static final int END_SEED = 100;
    private static final int TEST_NUMBER = 8;

    private int[] scores = new int[]{0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144};
    private int width;
    private int height;
    private String[] words;
    private HashSet<String> wordList;

    private void displayTestCase(long seed) {
        String ret = "seed is " + seed + "\n";
        ret += "width = " + width + ", height = " + height + "\n";
        ret += "Dictionary Length = " + words.length + "\n";
        if (words.length < 20)
            for (int i = 0; i < words.length; i++) ret += words[i] + "\n";
        System.out.println(ret);
    }

    private void displayPuzzle(String[] puzzle) {
        for (String line : puzzle) {
            System.out.println(line);
        }
    }

    public static void main(String[] args) throws IOException {
        final List<String> lines = new ArrayList<>();
        for (int i = START_SEED; i < END_SEED; i++) {
            final Result result = new TestExecutor().runTest(i);
            System.out.println(result.toString());
            lines.add(result.toString());
        }
        try {
            final Path path = Paths.get("resource\\test" + TEST_NUMBER + ".txt");
            Files.write(path, lines);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }

        ScoreCalculator.main(args);
    }

    private Result runTest(long seed) {
        readTestCase(seed);
        displayTestCase(seed);
        final long startTime = System.currentTimeMillis();
        final String[] puzzle = new CrosswordPuzzler().createPuzzle(width, height, words);
        final long endTime = System.currentTimeMillis();
        final double execTime = (endTime - startTime) / 1000.0;
        String[] ret = puzzle;
        if (ret.length != height) {
            displayPuzzle(puzzle);
            throw new IllegalStateException("Return value was expected to have " + height + " elements.");
        }
        int score = 0;
        for (int i = 0; i < ret.length; i++) {
            if (ret[i].length() != width) {
                displayPuzzle(puzzle);
                throw new IllegalStateException("Element " + i + " of return was expected to have " + width + " characters.");
            }
            for (int j = 0; j < ret[i].length(); j++) {
                char c = ret[i].charAt(j);
                if (c == '-') {
                    score--;
                    continue;
                }
                if (c < 'A' || c > 'Z') {
                    displayPuzzle(puzzle);
                    throw new IllegalStateException("Character " + j + " of element " + i + " of return is not a valid character.");
                }
            }
        }
        for (int i = 0; i < ret.length; i++) {
            String[] s = ret[i].split("-");
            for (int j = 0; j < s.length; j++) {
                if (s[j].length() > 1) {
                    if (wordList.contains(s[j])) {
                        wordList.remove(s[j]);
                        score += scores[s[j].length()];
                    } else {
                        displayPuzzle(puzzle);
                        throw new IllegalStateException("Horizontal word " + s[j] + " is not in the dictionary or is duplicated.");
                    }
                }
            }
        }
        for (int i = 0; i < ret[0].length(); i++) {
            String t = "";
            for (int k = 0; k < ret.length; k++) t += ret[k].charAt(i);
            String[] s = t.split("-");
            for (int j = 0; j < s.length; j++) {
                if (s[j].length() > 1) {
                    if (wordList.contains(s[j])) {
                        wordList.remove(s[j]);
                        score += scores[s[j].length()];
                    } else {
                        displayPuzzle(puzzle);
                        throw new IllegalStateException("Vertical word " + s[j] + " is not in the dictionary or is duplicated.");
                    }
                }
            }
        }

//        displayPuzzle(puzzle);
        return new Result(seed, Math.max(0, score), execTime);
    }

    private void readTestCase(long seed) {
        try {
            final Path path = Paths.get("C:\\Users\\Madao\\MM109\\test\\" + seed + ".txt");
            final List<String> list = Files.readAllLines(path);
            width = Integer.parseInt(list.get(0));
            height = Integer.parseInt(list.get(1));
            words = new String[list.size() - 2];
            for (int i = 2; i < list.size(); i++) {
                words[i - 2] = list.get(i);
            }
            wordList = new HashSet<>();
            for (String word : words) {
                wordList.add(word);
            }
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private static class Result {
        private final long seed;
        private final int score;
        private final double execTime;

        private Result(long seed, int score, double execTime) {
            this.seed = seed;
            this.score = score;
            this.execTime = execTime;
        }

        @Override
        public String toString() {
            String res = "";
            res += String.format("seed: %5s,", seed);
            res += "\t";
            res += String.format("score: %6d,", score);
            res += "\t";
            res += String.format("time: %3.2f,", execTime);
            return res;
        }
    }
}
